import React, { useReducer } from 'react';

import slideData from "../slideData"
import { reducer, initialState} from "../useReduce"

import "./index.css"

const  Slide = () =>{

 const [state, dispatch] = useReducer(reducer, initialState)

      return(
        <React.Fragment>
         <div className="slide-contain">
             <div className="slideshow">
                <img  src={slideData[state.slideIndex].image}    alt="text"/>  
                <div  className="pagation">  
                  <i  className='fas fa-angle-left'
                      onClick={()=>dispatch({type:'prevSlide'})}></i>   
                    {state.slideData.map((item, index)=>{
                          return(
                            <img  
                              alt="text"  
                              key={index}
                              src={item.image}  
                              onClick={() => dispatch({type: "changeSlide", index : index})}
                              className = {state.slideIndex === index ? "active" : "slide-item"}
                              />
                    )})}   
                  <i className='fas fa-angle-right'
                    onClick={()=> dispatch({type:"nextSlide"})}></i> 
                </div> 
             </div>     
         </div>
  
       </React.Fragment> 
     )
  
}
export default Slide