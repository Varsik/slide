import slideData from "../slideData"

export const initialState = {
    slideIndex: 0,
    slideData : slideData
};
export const reducer = (state, action) =>{
    switch(action.type){

        case 'changeSlide':
           return {
             ...state,
             slideIndex: action.index,
           }
        case 'nextSlide':
          return{
            ...state,
             slideIndex: state.slideIndex >  state.slideData.length - 2 ? 0 :  ++state.slideIndex,
          }   
        case 'prevSlide':
            return{
              ...state,
               slideIndex:  state.slideIndex  === 0 ? state.slideData.length - 1  : --state.slideIndex,
        } 
        default :
            return state        
    }
};
